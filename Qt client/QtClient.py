import socket
import time
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QTimer
from Interface import Ui_MainWindow
from threading import Thread
from time import sleep

HEADER_LENGTH = 10

#IP = "127.0.0.1"
IP=input("IP to connect to: ")
PORT = 8081
my_username = input("Username: ")
#my_username = "test_GUI"

# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP, connectionless, datagrams, socket.SOCK_RAW - raw IP packets
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect to a given ip and port
client_socket.connect((IP, PORT))

# Set connection to non-blocking state, so .recv() call won;t block, just return some exception we'll handle
client_socket.setblocking(False)

# Prepare username and header and send them
# We need to encode username to bytes, then count number of bytes and prepare header of fixed size, that we encode to bytes as well
username = my_username.encode('utf-8')
username_header = f"{len(username):<{HEADER_LENGTH}}".encode('utf-8')
client_socket.send(username_header + username)
client_socket.setblocking(1)

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Define event handlers:
        new_messages = []
        
        def fetch_new_messages():
            while True:
                sleep(0.1)
                # Receive our "header" containing username length, it's size is defined and constant
                username_header = client_socket.recv(HEADER_LENGTH)

                # If we received no data, server gracefully closed a connection, for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
                if not len(username_header):
                    print('Connection closed by the server')
                    sys.exit()

                # Convert header to int value
                username_length = int(username_header.decode('utf-8').strip())

                # Receive and decode username
                username = client_socket.recv(username_length).decode('utf-8')

                # Now do the same for message (as we received username, we received whole message, there's no need to check if it has any length)
                message_header = client_socket.recv(HEADER_LENGTH)
                message_length = int(message_header.decode('utf-8').strip())
                                
                message = client_socket.recv(message_length).decode('utf-8')
                output = (f'{username} > {message}')
                print(output)
                self.ui.textBrowser.append(output)
                
        thread = Thread(target=fetch_new_messages, daemon=True)
        thread.start()
                
        def send_message():
            message = self.ui.lineEdit.text()

            if message:
                # Encode message to bytes, prepare header and convert to bytes, like for username above, then send
                self.ui.textBrowser.append(message)
                self.ui.lineEdit.setText('')
                message = message.encode('utf-8')
                message_header = f"{len(message):<{HEADER_LENGTH}}".encode('utf-8')
                client_socket.send(message_header + message)
                

        self.ui.lineEdit.returnPressed.connect(send_message)
        self.ui.pushButton.clicked.connect(send_message)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
